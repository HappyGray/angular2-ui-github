import { provideRouter, RouterConfig } from '@angular/router';

import { PageNotFoundComponent } from '../../components/page-not-found/page-not-found.component';
import { PageIssuesRoutes } from '../../components/page-issues/page-issues.routes';

export const routes: RouterConfig = [
    {
        path: '404',
        component: PageNotFoundComponent
    },
    {
        path: '*path',
        redirectTo: '/404'
    },
    ...PageIssuesRoutes
];

export const APP_ROUTER_PROVIDERS = [
    provideRouter(routes)
];