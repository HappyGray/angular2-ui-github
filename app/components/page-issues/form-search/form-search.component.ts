import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'form-search',
    templateUrl: './app/components/page-issues/form-search/form-search.component.html',
    styleUrls: ['./app/components/page-issues/form-search/form-search.component.css']
})
export class FormSearchComponent {
    @Input() owner:string;
    @Input() repo:string;

    @Output() onsubmit:EventEmitter<Object>;

    constructor() {
        this.onsubmit = new EventEmitter<Object>();
    }

    searchRepositories(owner: string) {
    }

    onclick(owner: string, repo:string) {
        if (owner && repo) {
            this.onsubmit.emit({owner: owner, repo: repo});
        }
    }

}