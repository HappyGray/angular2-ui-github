import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute } from '@angular/router';

import { FormSearchComponent } from  '../../components/page-issues/form-search/form-search.component';
import { IssueService } from '../../services/github-issue/github-issue.service';

@Component({
    selector: 'page-issues',
    templateUrl: './app/components/page-issues/page-issues.component.html',
    styleUrls: ['./app/components/page-issues/page-issues.component.css'],
    providers: [IssueService],
    directives: [ROUTER_DIRECTIVES, FormSearchComponent]
})
export class PageIssuesComponent implements OnInit {

    // Контейнеры для вывода
    issues:Object[] = [];
    pagination:Object[] = [];

    // Параметры для ввода/вывода
    owner:string = '';
    repo:string = '';
    pageIndex:number = 1;

    // Состояния
    private loading:boolean = false;
    private error_status:number = 0;

    // Subscription
    private sub:any = null;
    private querySub:any = null;

    constructor(private issueService: IssueService, private router: Router, private route: ActivatedRoute) {}

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {

            // Обнулить состояние ошибок
            this.error_status = 0;

            // Получить параметры
            this.owner = params['owner'] ? params['owner'] : '';
            this.repo = params['repo'] ? params['repo'] : '';

            this.querySub = this.router.routerState.queryParams.subscribe(queryParams => {
                this.pageIndex = parseInt(queryParams['page']) ? parseInt(queryParams['page']) : 1;

                if (this.owner && this.repo) {
                    this.loading = true;

                    this.issueService
                        .findIssues(this.owner, this.repo, this.pageIndex)
                        .then(data => {
                            this.issues = data['items'];
                            this.pagination = [];

                            console.log(data);

                            if ( ! this.issues.length) {
                                this.router.navigateByUrl(`/${this.owner}/${this.repo}/issues?page=${data['total_count']}`);
                            }

                            let total_count_ceil = Math.ceil(data['total_count']/2);
                            for (let i = 1; i <= data['total_count']; i++) {

                                // if (
                                //     data['total_count'] < 10 ||
                                //     (data['total_count'] >= 10 && (
                                //         (i <= 5 || i > data['total_count']-5) //||
                                //         // (i >= (total_count_ceil - 2) && i <= (total_count_ceil + 2))
                                //     ))
                                // ) {
                                    this.pagination.push({
                                        title: `${i}`,
                                        index: i
                                    });
                                // }
                                // if (data['total_count'] >= 10 && total_count_ceil == i) {
                                //     this.pagination.push({
                                //         title: '...',
                                //         index: i
                                //     });
                                // }
                            }

                            // console.log(this.pagination)
                            this.loading = false;
                        })
                        .catch(error => {
                            this.issues = [];
                            this.pagination = [];
                            this.loading = false;
                            this.error_status = error.status;
                        });
                }
            });
        } );
    }

    ngOnDestroy() {
        if (this.sub != null) {
            this.sub.unsubscribe();
        }
        if (this.querySub != null) {
            this.querySub.unsubscribe();
        }
    }

    onFormSubmit(params: Object): void {
        // Переадресация на страницу с issues
        this.router.navigateByUrl(`/${params['owner']}/${params['repo']}/issues`);
    }

    /**
     * Вывод сообщения о состоянии
     *
     * @returns {string}
     */
    get message() {
        let message = '';

        if (this.error_status == 403) {
            message = 'Доступ к данным заблокирован';
        }
        else if (this.error_status == 404) {
            message = 'Страницы не существует';
        }
        else if (this.loading) {
            message = 'Загрузка...';
        }
        else if (this.owner && this.repo && this.issues.length == 0) {
            message = 'У репозитория нет issues';
        }

        return message;
    }

}