import { RouterConfig } from '@angular/router';

import { PageIssuesComponent } from '../../components/page-issues/page-issues.component';

export const PageIssuesRoutes: RouterConfig = [
    {
        path: '',
        component: PageIssuesComponent
    },
    {
        path: ':owner/:repo/issues',
        component: PageIssuesComponent
    }
];