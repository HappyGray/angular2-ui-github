import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

// Todo: Создать интерфейс для user

@Injectable()
export class UserService {
    private apiUrl = 'https://api.github.com';

    constructor(private http: Http) {}

    getUser(owner:string, repo:string, page:number = 1, per_page:number = 30): Promise<Object> {
        let url = `${this.apiUrl}/repos/${owner}/${repo}/issues?page=${page}&per_page=${per_page}`;

        return this.http.get(url)
            .toPromise()
            .then( res => {
                return {
                    items: res.json(),
                    count_pages: Number(res.headers.get('Link').split(';')[1].match(/page=(\d+).*$/)[1])
                }
            } )
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.log('Error when requesting by api GitHub:', error);
        return Promise.reject(error.message || error);
    }

}