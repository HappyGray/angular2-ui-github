import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

// Todo: Создать интерфейс для user

@Injectable()
export class SearchService {
    private apiUrl = 'https://api.github.com';

    constructor(private http: Http) {}

    findUser(login:string): Promise<Object> {
        let url = `${this.apiUrl}/search/users?q=${login}`;

        return this.http.get(url)
            .toPromise()
            .then( res => {
                return {
                    items: res.json(),
                    count_pages: Number(res.headers.get('Link').split(';')[1].match(/page=(\d+).*$/)[1])
                }
            } )
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.log('Error when requesting by api GitHub:', error);
        return Promise.reject(error.message || error);
    }

}