import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

// Todo: Создать интерфейс для issue

@Injectable()
export class IssueService {
    private apiUrl = 'https://api.github.com';

    constructor(private http: Http) {}

    findIssues(owner:string, repo:string, page_index:number = 1, per_page:number = 30): Promise<Object> {
        let url = `${this.apiUrl}/repos/${owner}/${repo}/issues?page=${page_index}&per_page=${per_page}`;

        console.log('GET:', url);

        return this.http.get(url)
            .toPromise()
            .then( res => {
                let total_count = 1;

                if (res.headers.get('Link') != null) {
                    console.log(res.headers.get('Link'));

                    let noLinkLast = true;

                    res.headers.get('Link').split(',').forEach(link => {
                        if (link.indexOf('last') > -1) {
                            total_count = parseInt(link.match(/page=(\d+).*$/)[1]);
                            noLinkLast = false;
                        }
                    });

                    if (noLinkLast) {
                        total_count = page_index;
                    }
                }

                return {
                    items: res.json(),
                    per_page: per_page,
                    page_index: page_index,
                    total_count: total_count
                }
            } )
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.log('Error when requesting by api GitHub:', error);
        return Promise.reject(error.message || error);
    }

}